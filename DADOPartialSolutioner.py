import json
from typing import Union, Any
import pandas as pd
from DADOChangeAnalysis import ChangeAnalyzer

class PartialSolutioner:

    def __init__(self, prev_dadojson: "Union[str, dict]", curr_dadojson: "Union[str, dict]", prev_sol: "Union[pd.DataFrame, str]") -> None:
        if type(prev_sol) == str:
            prev_sol = pd.read_csv(prev_sol)
        if type(curr_dadojson) == str:
            with open(curr_dadojson, 'r') as in_json:
                curr_setup = json.load(in_json)
        else:
            curr_setup = curr_dadojson
        self.__change_analyzer = ChangeAnalyzer(prev_dadojson, curr_dadojson)
        self.__changes = self.__change_analyzer.get_comparison()
        self.__curr_setup = curr_setup
        self.__last_wf_ms = [(wf['id'], wf['chain'][-1]) for wf in curr_setup['workflows']]
        self.__z_vars = self.__get_rich_z_vars(prev_sol)
        self.__f_vars = self.__get_rich_f_vars(prev_sol)
        self.__fprime_vars = self.__get_rich_fprime_vars(prev_sol)
        self.__cf_vars = self.__get_rich_cf_vars(prev_sol)
        self.__x_vars = self.__get_rich_x_vars(prev_sol)
        self.__y_vars = self.__get_rich_y_vars(prev_sol)
        self.__pss = prev_sol.size
    
    @staticmethod
    def __get_rich_z_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        z_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith('z_')].reset_index(drop=True)
        z_vars['TMP'] = z_vars['Variable'].str.split('_', n=3)
        z_vars['Host'] = z_vars['TMP'].str[1]
        z_vars['Workflow'] = z_vars['TMP'].str[2]
        z_vars['Microservice'] = z_vars['TMP'].str[3]
        z_vars = z_vars.loc[(z_vars['Host'] != 'prime') & (z_vars['Host'] != 'double')]
        z_vars.drop(['TMP'], axis=1, inplace=True)
        return z_vars
    
    @staticmethod
    def __get_rich_fprime_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        fprime_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'fprime_')].reset_index(drop=True)
        fprime_vars['TMP'] = fprime_vars['Variable'].str.split('_', n=4)
        fprime_vars['Source'] = fprime_vars['TMP'].str[1]
        fprime_vars['Destination'] = fprime_vars['TMP'].str[2]
        fprime_vars['Host'] = fprime_vars['TMP'].str[3]
        fprime_vars['Workflow'] = fprime_vars['TMP'].str[4]
        fprime_vars.drop(['TMP'], axis=1, inplace=True)
        return fprime_vars
    
    @staticmethod
    def __get_rich_f_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        f_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'f_')].reset_index(drop=True).copy()
        f_vars['TMP'] = f_vars['Variable'].str.split('_', n=5)
        f_vars['Source'] = f_vars['TMP'].str[1]
        f_vars['Destination'] = f_vars['TMP'].str[2]
        f_vars['Host'] = f_vars['TMP'].str[3]
        f_vars['Workflow'] = f_vars['TMP'].str[4]
        f_vars['Microservice'] = f_vars['TMP'].str[5]
        f_vars.drop(['TMP'], axis=1, inplace=True)
        return f_vars
    
    @staticmethod
    def __get_rich_cf_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        cf_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'cf_')].reset_index(drop=True).copy()
        cf_vars['TMP'] = cf_vars['Variable'].str.split('_', n=3)
        cf_vars['Source'] = cf_vars['TMP'].str[1]
        cf_vars['Destination'] = cf_vars['TMP'].str[2]
        cf_vars['Switch'] = cf_vars['TMP'].str[3]
        cf_vars.drop(['TMP'], axis=1, inplace=True)
        return cf_vars
    
    @staticmethod
    def __get_rich_x_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        x_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'x_')].reset_index(drop=True).copy()
        x_vars['TMP'] = x_vars['Variable'].str.split('_', n=1)
        x_vars['Switch'] = x_vars['TMP'].str[1]
        x_vars.drop(['TMP'], axis=1, inplace=True)
        return x_vars
    
    @staticmethod
    def __get_rich_y_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        y_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'y_')].reset_index(drop=True)
        y_vars['TMP'] = y_vars['Variable'].str.split('_', n=2)
        y_vars['Switch'] = y_vars['TMP'].str[1]
        y_vars['Controller'] = y_vars['TMP'].str[2]
        y_vars.drop(['TMP'], axis=1, inplace=True)
        return y_vars
    
    def __invalidate_workflows(self, workflow_ids: "list[str]") -> None:
        for wf_id in workflow_ids:
            self.__z_vars.drop(
                self.__z_vars.index[self.__z_vars["Workflow"] == wf_id], inplace=True)
            self.__f_vars.drop(
                self.__f_vars.index[self.__f_vars["Workflow"] == wf_id], inplace=True)
            self.__fprime_vars.drop(
                self.__fprime_vars.index[self.__fprime_vars["Workflow"] == wf_id], inplace=True)
    
    def __invalidate_switches(self, switch_ids: "list[str]") -> None:
        for sw_id in switch_ids:
            cnt_placed = bool(
                int(self.__x_vars.query("Switch == '{}'".format(sw_id))["Value"]))
            if cnt_placed:
                    self.__x_vars.drop(self.__x_vars.index[self.__x_vars["Switch"] == sw_id], inplace=True)
                    self.__y_vars.drop(self.__y_vars.index[(self.__y_vars["Switch"] == sw_id) | (
                        self.__y_vars["Controller"] == sw_id)], inplace=True)
                    self.__cf_vars.drop(self.__cf_vars.index[(
                        self.__cf_vars["Switch"] == sw_id)], inplace=True)
    
    def __invalidate_switch_flows(self, switch_ids: "list[str]") -> None:
        for sw_id in switch_ids:
            self.__f_vars.drop(
                self.__f_vars.index[(self.__f_vars["Source"] == sw_id) | (self.__f_vars["Destination"] == sw_id)], inplace = True)
            self.__fprime_vars.drop(
                self.__fprime_vars.index[(self.__fprime_vars["Source"] == sw_id) | (self.__fprime_vars["Destination"] == sw_id)], inplace=True)
            self.__cf_vars.drop(
                self.__cf_vars.index[(self.__cf_vars["Source"] == sw_id) | (self.__cf_vars["Destination"] == sw_id) | (self.__cf_vars["Switch"] == sw_id)], inplace=True)
    
    def __invalidate_links(self, link_descs: "list[tuple[str, str]]") -> None:
        for link in link_descs:
            self.__f_vars.drop(self.__f_vars.index[(self.__f_vars["Source"] == link[0]) & (
                self.__f_vars["Destination"] == link[1])], inplace=True)
            self.__f_vars.drop(self.__f_vars.index[(self.__f_vars["Source"] == link[1]) & (
                self.__f_vars["Destination"] == link[0])], inplace=True)
            self.__fprime_vars.drop(self.__fprime_vars.index[(self.__fprime_vars["Source"] == link[0]) & (
                self.__fprime_vars["Destination"] == link[1])], inplace=True)
            self.__fprime_vars.drop(self.__fprime_vars.index[(self.__fprime_vars["Source"] == link[1]) & (
                self.__fprime_vars["Destination"] == link[0])], inplace=True)
            self.__cf_vars.drop(self.__cf_vars.index[(self.__cf_vars["Source"] == link[0]) & (
                self.__cf_vars["Destination"] == link[1])], inplace=True)
            self.__cf_vars.drop(self.__cf_vars.index[(self.__cf_vars["Source"] == link[1]) & (
                self.__cf_vars["Destination"] == link[0])], inplace=True)
    
    def __invalidate_hosts(self, host_ids: "list[str]") -> None:
        for host in host_ids:
            self.__f_vars.drop(
                self.__f_vars.index[self.__f_vars["Host"] == host], inplace=True)
            self.__fprime_vars.drop(
                self.__fprime_vars.index[self.__fprime_vars["Host"] == host], inplace=True)
            self.__z_vars.drop(
                self.__z_vars.index[self.__z_vars["Host"] == host], inplace=True)
    
    def __invalidate_microservices(self, ms_ids: "list[str]") -> None:
        wf_ms_by_id = {wf["id"]: wf["chain"]
                       for wf in self.__curr_setup["workflows"]}
        for ms_id in ms_ids:
            affected_wfs = {wfid: wf_ms_by_id[wfid] for wfid in wf_ms_by_id if ms_id in wf_ms_by_id[wfid]}
            wf_next_by_id = {wfid: affected_wfs[wfid][i+1] for wfid in affected_wfs for i, x in enumerate(
                affected_wfs[wfid]) if x == ms_id and i+1 < len(affected_wfs[wfid])}
            self.__f_vars.drop(
                self.__f_vars.index[self.__f_vars["Microservice"] == ms_id], inplace=True)
            for wf_id, sec_ms_id in wf_next_by_id.items():
                self.__f_vars.drop(
                    self.__f_vars.index[(self.__f_vars["Microservice"] == sec_ms_id) and 
                    (self.__f_vars["Workflow"] == wf_id)], inplace=True)
            self.__z_vars.drop(
                self.__z_vars.index[self.__z_vars["Microservice"] == ms_id], inplace=True)
    
    def __invalidate_returnal(self, wf_ids: "list[str]") -> None:
        for wf_id in wf_ids:
            self.__fprime_vars.drop(
                self.__fprime_vars.index[self.__fprime_vars["Workflow"] == wf_id], inplace=True
            )
    
    def __invalidate_deployment(self, wf_ms_host_ids: "list[tuple[str, str, str]]") -> None:
        ret_inv = []
        wf_ms_by_id = {wf["id"]: wf["chain"]
                       for wf in self.__curr_setup["workflows"]}
        for wf, ms, host in wf_ms_host_ids:
            self.__f_vars.drop(
                self.__f_vars.index[(self.__f_vars["Microservice"] == ms) & (
                    self.__f_vars["Workflow"] == wf)
                    # & (self.__f_vars["Host"] == host)
                    ], inplace=True
            )
            self.__z_vars.drop(
                self.__z_vars.index[(self.__z_vars["Microservice"] == ms) & (
                    self.__z_vars["Workflow"] == wf)# & (self.__z_vars["Host"] == host)
                ], inplace=True
            )
            chain = wf_ms_by_id[wf]
            next_idx = chain.index(ms)+1
            if next_idx < len(chain):
                self.__f_vars.drop(
                    self.__f_vars.index[(self.__f_vars["Microservice"] == chain[next_idx]) & (
                        self.__f_vars["Workflow"] == wf)], inplace=True
                )
            else:
                ret_inv.append(wf)
        self.__invalidate_returnal(ret_inv)
    
    def __invalidate_control_route(self, switch_link_ids: "list[tuple[str, tuple[str, str]]]") -> None:
        for switch, link in switch_link_ids:
            self.__cf_vars.drop(
                self.__cf_vars.index[(self.__cf_vars["Switch"] == switch)# & (
                    #((self.__cf_vars["Source"] == link[0]) & (self.__cf_vars["Destination"] == link[1])
                    #) | (
                    #(self.__cf_vars["Source"] == link[1]) & (self.__cf_vars["Destination"] == link[0])
                    #))
                    ], inplace=True
            )

    def __invalidate_route(self, wf_ms_host_link_ids: "list[tuple[str, str, str, tuple[str, str]]]") -> None:
        for wf, ms, host, link in wf_ms_host_link_ids:
            self.__f_vars.drop(
                self.__f_vars.index[(self.__f_vars["Workflow"] == wf) & (self.__f_vars["Microservice"] == ms) #& (self.__f_vars["Host"] == host) & (
                #    ((self.__f_vars["Source"] == link[0]) & (self.__f_vars["Destination"] == link[1])
                #     ) | (
                #        (self.__f_vars["Source"] == link[1]) & (
                #            self.__f_vars["Destination"] == link[0])
                #    ))
                ], inplace=True
            )
    
    def __invalidate_ret_route(self, wf_host_link_ids: "list[tuple[str, str, tuple[str, str]]]") -> None:
        for wf, host, link in wf_host_link_ids:
            self.__fprime_vars.drop(
                self.__fprime_vars.index[(self.__fprime_vars["Workflow"] == wf) #& (self.__fprime_vars["Host"] == host) & (
                #    ((self.__fprime_vars["Source"] == link[0]) & (self.__fprime_vars["Destination"] == link[1])
                #     ) | (
                #        (self.__fprime_vars["Source"] == link[1]) & (
                #            self.__fprime_vars["Destination"] == link[0])
                #    ))
                ], inplace=True
            )
    
    def __clear_partial_limitations(self) -> None:
        # All microservices that aren't deployed must not limit where to migrate
        for workflow in self.__curr_setup["workflows"]:
            wf_id = workflow["id"]
            for ms_id in workflow["chain"]:
                z_debug = self.__z_vars.query(
                    "Workflow == @wf_id and Microservice == @ms_id and Value > 0")
                if len(self.__z_vars.query("Workflow == @wf_id and Microservice == @ms_id and Value > 0")) == 0:
                    self.__z_vars.drop(self.__z_vars.index[(self.__z_vars["Workflow"] == wf_id) &
                        (self.__z_vars["Microservice"] == ms_id)
                    ], inplace=True)
                # Same goes for routes
                f_debug = self.__f_vars.query(
                    "Workflow == @wf_id and Microservice == @ms_id and Value > 0")
                if len(self.__f_vars.query("Workflow == @wf_id and Microservice == @ms_id and Value > 0")) == 0:
                    self.__f_vars.drop(self.__f_vars.index[(self.__f_vars["Workflow"] == wf_id) &
                        (self.__f_vars["Microservice"] == ms_id)
                    ], inplace=True)
            fprime_debug = self.__fprime_vars.query("Workflow == @wf_id and Value > 0")
            if len(self.__fprime_vars.query("Workflow == @wf_id and Value > 0")) == 0:
                self.__fprime_vars.drop(self.__fprime_vars.index[self.__fprime_vars["Workflow"] == wf_id], inplace=True)
        # It's nonsense to keep limits on where not to put controllers
        self.__x_vars.drop(self.__x_vars.index[self.__x_vars["Value"] <= 0], inplace=True)
        # Routes and non-assigned switches have no reason to be kept either
        for switch in self.__curr_setup["switches"]:
            s_id = switch["id"]
            if len(self.__y_vars.query("Switch == @s_id and Value > 0")) == 0:
                self.__y_vars.drop(self.__y_vars.index[self.__y_vars["Switch"] == s_id], inplace=True)
            if len(self.__cf_vars.query("Switch == @s_id and Value > 0")) == 0:
                self.__cf_vars.drop(self.__cf_vars.index[self.__cf_vars["Switch"] == s_id], inplace=True)

    def __check_capex_budget(self) -> None:
        budget = self.__curr_setup["parameters"].get("capex_budget", 0)
        hosts = self.__curr_setup["hosts"]
        links = self.__curr_setup["links"]
        switches = self.__curr_setup["switches"]
        controllers = list(self.__x_vars.loc[self.__x_vars["Value"] > 0]["Switch"])
        for cnt in controllers:
            cnt["capex"] = cnt.get("capex_cnt", 0)
        host_tuples = [("host", h) for h in hosts]
        link_tuples = [("link", l) for l in links]
        switch_tuples = [("switch", s) for s in switches]
        controller_tuples = [("controller", c) for c in controllers]
        sorted_elements = sorted(host_tuples+link_tuples+switch_tuples+controller_tuples, key=lambda e: (e[1].get("migration_cost_cnt", 0), -e[1].get("capex", 0)))
        hosts_invalidated = []
        links_invalidated = []
        switches_invalidated = []
        controllers_invalidated = []
        wf_starters = list(dict.fromkeys([wf["starter"] for wf in self.__curr_setup["workflows"]]))
        for el_kind, element in sorted_elements:
            if el_kind == "host":
                host = element
                if len(self.__z_vars.loc[(self.__z_vars["Host"] == host["id"]) & (self.__z_vars["Value"] > 0)]) or host["id"] in wf_starters:
                    host_capex = host.get("capex", 0)
                    if budget >= host_capex:
                        budget -= host_capex
                    else:
                        hosts_invalidated.append(host["id"])
            elif el_kind == "switch":
                switch = element
                all_usages = len(self.__f_vars.loc[((self.__f_vars["Source"] == switch["id"]) | (
                    self.__f_vars["Destination"] == switch["id"])) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[((self.__cf_vars["Source"] == switch["id"]) | (
                    self.__cf_vars["Destination"] == switch["id"])) & (self.__cf_vars["Value"] > 0)
                    & (self.__cf_vars["Switch"] != switch["id"])])
                if all_usages > 0:
                    switch_capex = switch.get("capex", 0)
                    if budget >= switch_capex:
                        budget -= switch_capex
                    else:
                        switches_invalidated.append(switch["id"])
            elif el_kind == "controller":
                controller = element
                if len(self.__y_vars.loc[(self.__y_vars["Controller"] == controller["id"]) & (self.__y_vars["Value"] > 0) & (self.__y_vars["Switch"] != controller["id"])]) > 0:
                    controller_capex = controller.get("capex", 0)
                    if budget >= controller_capex:
                        budget -= controller_capex
                    else:
                        controllers_invalidated.append(controller["id"])
            elif el_kind == "link":
                link = element
                all_usages = len(self.__f_vars.loc[(self.__f_vars["Source"] == link["source"]) & (
                    self.__f_vars["Destination"] == link["destination"]) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__f_vars.loc[(self.__f_vars["Source"] == link["destination"]) & (
                    self.__f_vars["Destination"] == link["source"]) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[(self.__cf_vars["Source"] == link["source"]) & (
                    self.__cf_vars["Destination"] == link["destination"]) & (self.__cf_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[(self.__cf_vars["Source"] == link["destination"]) & (
                    self.__cf_vars["Destination"] == link["source"]) & (self.__cf_vars["Value"] > 0)])
                if all_usages > 0:
                    link_capex = link.get("capex", 0)
                    if budget >= link_capex:
                        budget -= link_capex
                    else:
                        links_invalidated.append((link["source"], link["destination"]))
        self.__invalidate_hosts(hosts_invalidated)
        self.__invalidate_switch_flows(switches_invalidated)
        self.__invalidate_switches(controllers_invalidated)
        self.__invalidate_links(links_invalidated)
                

    def __check_opex_budget(self) -> None:
        budget = self.__curr_setup["parameters"].get("opex_budget", 0)
        hosts = self.__curr_setup["hosts"]
        links = self.__curr_setup["links"]
        switches = self.__curr_setup["switches"]
        controllers = list(
            self.__x_vars.loc[self.__x_vars["Value"] > 0]["Switch"])
        for cnt in controllers:
            cnt["opex"] = cnt.get("opex_cnt", 0)
        host_tuples = [("host", h) for h in hosts]
        link_tuples = [("link", l) for l in links]
        switch_tuples = [("switch", s) for s in switches]
        controller_tuples = [("controller", c) for c in controllers]
        sorted_elements = sorted(host_tuples+link_tuples+switch_tuples +
                                 controller_tuples, key=lambda e: (e[1].get('migration_cost_cnt', 0), -e[1].get("opex", 0)))
        hosts_invalidated = []
        links_invalidated = []
        switches_invalidated = []
        controllers_invalidated = []
        wf_starters = list(dict.fromkeys(
            [wf["starter"] for wf in self.__curr_setup["workflows"]]))
        for el_kind, element in sorted_elements:
            if el_kind == "host":
                host = element
                key_vals = self.__z_vars.loc[(self.__z_vars["Host"] == host["id"]) & (
                    self.__z_vars["Value"] > 0)]
                if len(key_vals) or host["id"] in wf_starters:
                    ms_cycles = {ms["id"]: ms["cycles"] for ms in self.__curr_setup["microservices"]}
                    ms_memory = {ms["id"]: ms["memory"] for ms in self.__curr_setup["microservices"]}
                    dep_ms = list(key_vals["Microservice"])
                    total_cycles = sum([ms_cycles[ms] for ms in dep_ms])
                    total_memory = sum([ms_memory[ms] for ms in dep_ms])
                    host_opex = host.get("opex_cycle", 0) * total_cycles + host.get("opex_memory", 0) * total_memory
                    if budget >= host_opex:
                        budget -= host_opex
                    else:
                        hosts_invalidated.append(host["id"])
            elif el_kind == "switch":
                switch = element
                all_usages = len(self.__f_vars.loc[((self.__f_vars["Source"] == switch["id"]) | (
                    self.__f_vars["Destination"] == switch["id"])) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[((self.__cf_vars["Source"] == switch["id"]) | (
                    self.__cf_vars["Destination"] == switch["id"])) & (self.__cf_vars["Value"] > 0)
                    & (self.__cf_vars["Switch"] != switch["id"])])
                if all_usages > 0:
                    switch_opex = switch.get("opex", 0)
                    if budget >= switch_opex:
                        budget -= switch_opex
                    else:
                        switches_invalidated.append(switch["id"])
            elif el_kind == "controller":
                controller = element
                if len(self.__y_vars.loc[(self.__y_vars["Controller"] == controller["id"]) & (self.__y_vars["Value"] > 0) & (self.__y_vars["Switch"] != controller["id"])]) > 0:
                    controller_opex = controller.get("opex", 0)
                    if budget >= controller_opex:
                        budget -= controller_opex
                    else:
                        controllers_invalidated.append(controller["id"])
            elif el_kind == "link":
                link = element
                all_usages = len(self.__f_vars.loc[(self.__f_vars["Source"] == link["source"]) & (
                    self.__f_vars["Destination"] == link["destination"]) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__f_vars.loc[(self.__f_vars["Source"] == link["destination"]) & (
                    self.__f_vars["Destination"] == link["source"]) & (self.__f_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[(self.__cf_vars["Source"] == link["source"]) & (
                    self.__cf_vars["Destination"] == link["destination"]) & (self.__cf_vars["Value"] > 0)])
                all_usages += len(self.__cf_vars.loc[(self.__cf_vars["Source"] == link["destination"]) & (
                    self.__cf_vars["Destination"] == link["source"]) & (self.__cf_vars["Value"] > 0)])
                if all_usages > 0:
                    link_opex = link.get("opex", 0)
                    if budget >= link_opex:
                        budget -= link_opex
                    else:
                        links_invalidated.append(
                            (link["source"], link["destination"]))
        self.__invalidate_hosts(hosts_invalidated)
        self.__invalidate_switch_flows(switches_invalidated)
        self.__invalidate_switches(controllers_invalidated)
        self.__invalidate_links(links_invalidated)

    def __check_memory(self, host: str) -> None:
        deployed_df = self.__z_vars.loc[(self.__z_vars["Host"] == host) & (
            self.__z_vars["Value"] > 0)]
        ms_memory = {ms["id"]: ms["memory"]
                     for ms in self.__curr_setup["microservices"]}
        ms_mig_cost =  {ms["id"]: ms.get("migration_cost", 0)
                     for ms in self.__curr_setup["microservices"]}
        dep_ms = sorted(list(deployed_df[["Workflow", "Microservice"]].to_records(index=False)), key=lambda ms: (ms_mig_cost[ms[1]], -ms_memory[ms[1]]))
        total_memory = sum([ms_memory[ms[1]] for ms in dep_ms])
        host_mem = {h["id"]: h["memory"] for h in self.__curr_setup["hosts"]}
        dep_to_invalidate = []
        while host_mem[host] < total_memory:
            top_mem = ms_memory[dep_ms[0][1]]
            dep_to_invalidate.append((dep_ms[0][0], dep_ms[0][1], host))
            dep_ms = dep_ms[1:]
            total_memory -= top_mem
        self.__invalidate_deployment(dep_to_invalidate)
    
    def __check_qos(self) -> None:
        host_spd = {h["id"]: h["power"] for h in self.__curr_setup["hosts"]}
        ms_cycles = {ms["id"]: ms["cycles"] for ms in self.__curr_setup["microservices"]}
        lnk_lat = {(lnk["source"], lnk["destination"]): lnk["latency"] for lnk in self.__curr_setup["links"]}
        wf_to_invalidate = []
        for wf in self.__curr_setup["workflows"]:
            max_rt = wf["max_response_time"]
            wf_id = wf["id"]
            val_rt = True
            wf_rt = 0
            ms_df = self.__z_vars.query("Value > 0 and Workflow == @wf_id")
            flows_df = self.__f_vars.query("Value > 0 and Workflow == @wf_id")
            ms_dep = list(ms_df[["Microservice", "Host"]].to_records(index=False))
            for ms_id, h_id in ms_dep:
                wf_rt += ms_cycles[ms_id]/host_spd[h_id]
                if wf_rt > max_rt:
                    wf_to_invalidate.append(wf["id"])
                    val_rt = False
                    break
            if not val_rt:
                continue
            flow_dep = list(flows_df[["Source", "Destination"]].to_records(index=False))
            for src, dst in flow_dep:
                wf_rt += lnk_lat.get((src, dst), 0) + lnk_lat.get((dst, src), 0)
                if wf_rt > max_rt:
                    wf_to_invalidate.append(wf["id"])
                    val_rt = False
                    break
                if dst not in host_spd:
                    cflows_df = self.__cf_vars.query("Value > 0 and Switch == @dst")
                    ctrl_flow_dep = list(cflows_df[["Source", "Destination"]].to_records(index=False))
                    for csrc, cdst in ctrl_flow_dep:
                        wf_rt += lnk_lat.get((csrc, cdst), 0) + \
                            lnk_lat.get((cdst, csrc), 0)
                        if wf_rt > max_rt:
                            wf_to_invalidate.append(wf["id"])
                            val_rt = False
                            break
            if not val_rt:
                continue
        self.__invalidate_workflows(wf_to_invalidate)


    def __check_ms_memory(self, microservice: str) -> None:
        for host_dep in self.__z_vars.loc[(self.__z_vars["Microservice"] == microservice) & (
            self.__z_vars["Value"] > 0)]["Host"].unique():
            self.__check_memory(host_dep)

    def __check_link_capacity(self, link: "tuple[str, str]") -> None:
        passing_flows = self.__f_vars.loc[(((self.__f_vars["Source"] == link[0]) & (self.__f_vars["Destination"] == link[1])) | (
            (self.__f_vars["Source"] == link[1]) & (self.__f_vars["Destination"] == link[0]))) & (self.__f_vars["Value"] > 0)]
        passing_ret_flows = self.__fprime_vars.loc[(((self.__fprime_vars["Source"] == link[0]) & (self.__fprime_vars["Destination"] == link[1])) | (
            (self.__fprime_vars["Source"] == link[1]) & (self.__fprime_vars["Destination"] == link[0]))) & (self.__fprime_vars["Value"] > 0)]
        passing_control_flows = self.__cf_vars.loc[(((self.__cf_vars["Source"] == link[0]) & (self.__cf_vars["Destination"] == link[1])) | (
            (self.__cf_vars["Source"] == link[1]) & (self.__cf_vars["Destination"] == link[0]))) & (self.__cf_vars["Value"] > 0)]
        tuple_passing_flows = [tuple(["traffic"] + list(fl)) for fl in list(passing_flows[["Workflow", "Microservice", "Host"]].to_records(index=False))]
        tuple_passing_ret_flows = [tuple(["returnal"] + list(fl)) for fl in list(
            passing_ret_flows[["Workflow", "Host"]].to_records(index=False))]
        tuple_passing_control_flows = [(self.__curr_setup["parameters"]["control_size"], "control", fl) for fl in list(passing_control_flows["Switch"])]
        io_by_ms = {ms["id"]: (ms["input"], ms["output"]) for ms in self.__curr_setup["microservices"]}
        io_by_wf = {wf["id"]: ms["output"] for ms in self.__curr_setup["microservices"] for wf in self.__curr_setup["workflows"] if (wf["id"], ms["id"]) in self.__last_wf_ms}
        tuple_passing_flows = [tuple([io_by_ms[t[3]]] + list(t)) for t in tuple_passing_flows]
        tuple_passing_ret_flows = [tuple([io_by_wf[t[2]]] + list(t)) for t in tuple_passing_flows]
        all_flows = sorted(tuple_passing_flows+tuple_passing_ret_flows+tuple_passing_control_flows, key=lambda t: t[0])
        cap_by_lnk = {(lnk["source"], lnk["destination"]): lnk["capacity"] for lnk in self.__curr_setup["links"]}
        lnk_cap = cap_by_lnk.get(link, 0) + cap_by_lnk.get((link[1], link[0]), 0)
        cnt_flows_to_inval = []
        flows_to_inval = []
        ret_flows_to_inval = []
        for flow in all_flows:
            size = flow[0]
            if size <= lnk_cap:
                lnk_cap-= size
            else:
                kind = flow[1]
                params = flow[2:]
                if kind == "control":
                    cnt_flows_to_inval.append((params[0], link))
                elif kind == "traffic":
                    flows_to_inval.append((params[0], params[1], params[2], link))
                else:
                    ret_flows_to_inval.append((params[0], params[1], link))
        self.__invalidate_control_route(cnt_flows_to_inval)
        self.__invalidate_route(flows_to_inval)
        self.__invalidate_ret_route(ret_flows_to_inval)
        

    def __reduce_controllers(self, cnt_num: int) -> None:
        controllers = self.__x_vars.loc[self.__x_vars["Value"] > 0]
        cnt_mig_cost = {sw["id"]: sw["migration_cost_cnt"]
                     for sw in self.__curr_setup["switches"]}
        cont_num = len(controllers)
        if cont_num > cnt_num:
            cnts = sorted(list(controllers["Switch"]), key=lambda cnt: (cnt_mig_cost[cnt], len(self.__y_vars.loc[self.__y_vars["Controller"] == cnt])))
            rem_cnts = cnts[:cont_num-cnt_num]
            self.__invalidate_switches(rem_cnts)

    def invalidate(self):
        self.__invalidate_workflows(self.__changes["removed"].get("workflows", []))
        self.__invalidate_switches(self.__changes["removed"].get("switches", []))
        self.__invalidate_links(self.__changes["removed"].get("links", []))
        self.__invalidate_hosts(self.__changes["removed"].get("hosts", []))
        self.__invalidate_microservices(self.__changes["removed"].get("microservices", []))
        wf_ms_by_id = {wf["id"]: wf["chain"] for wf in self.__curr_setup["workflows"]}
        last_inval = list(filter(
            lambda k:  wf_ms_by_id[k][-1] in self.__changes["removed"].get("microservices", []), wf_ms_by_id.keys()))
        self.__invalidate_returnal(last_inval)

        invalid_wfs = []
        for wf_id in wf_ms_by_id:
            for ms_id in self.__changes["removed"].get("microservices", []):
                if ms_id in wf_ms_by_id[wf_id]:
                    invalid_wfs.append(wf_id)
                    break
        self.__invalidate_workflows(invalid_wfs)
        self.__invalidate_returnal(invalid_wfs)
        if "capex_budget" in self.__curr_setup["parameters"]:
            found = False
            for element in self.__changes["modified"]:
                for inner_element in self.__changes["modified"][element]:
                    if inner_element.get("capex", 0) > 0 or inner_element.get("capex_cnt", 0) > 0:
                        self.__check_capex_budget()
                        found = True
                        break
                if found:
                    break
        if "opex_budget" in self.__curr_setup["parameters"]:
            found = False
            for element in self.__changes["modified"]:
                for inner_element in self.__changes["modified"][element]:
                    if inner_element.get("opex", 0) > 0 or inner_element.get("opex_cnt", 0) > 0:
                        self.__check_opex_budget()
                        found = True
                        break
                if found:
                    break
        for host in self.__changes["modified"].get("hosts", []):
            if host.get("memory", 0) < 0:
                self.__check_memory(host["host"])
        for microservice in self.__changes["modified"].get("microservices", []):
            if microservice.get("memory", 0) > 0:
                self.__check_ms_memory(microservice["microservice"])
        for link in self.__changes["modified"]["links"]:
            if link.get("capacity", 0) < 0:
                self.__check_link_capacity(link["link"])
        mod_par_by_key = {}
        for inner_dict in self.__changes["modified"].get("parameters", []):
            mod_par_by_key.update(inner_dict)
        if mod_par_by_key.get("controllers", 0) < 0 or "controllers" in self.__changes["added"].get("parameters", []):
            self.__reduce_controllers(self.__curr_setup["parameters"]["controllers"])
        self.__check_qos()
        self.__clear_partial_limitations()
    
    def export_valid_solution(self, debug_str: bool = False) -> pd.DataFrame:
        valid_df = self.__z_vars[["Variable", "Value"]].append(
            self.__f_vars[["Variable", "Value"]]).append(
            self.__fprime_vars[["Variable", "Value"]]).append(
            self.__cf_vars[["Variable", "Value"]]).append(
            self.__x_vars[["Variable", "Value"]]).append(
            self.__y_vars[["Variable", "Value"]]
            ).copy()
        if debug_str:
            print('{} variables remained valid out of {} original variables'.format(valid_df.size, self.__pss))
        return valid_df

    def export_rich_solution_vars(self) -> "dict[str, pd.DataFrame]":
        return {
            'z': self.__z_vars.copy(),
            'f': self.__f_vars.copy(),
            'fprime': self.__fprime_vars.copy(),
            'cf': self.__cf_vars.copy(),
            'x': self.__x_vars.copy(),
            'y': self.__y_vars.copy()
        }
