from mip.model import *
from pandas.core.frame import DataFrame
from model import *
from time import perf_counter
import unmarshalling
import argparse
import pandas as pd

class PartialSolDADO:

    PARAMETERS_TAG = "parameters"
    CONTROLLERS_TAG = "controllers"
    CONTROL_SIZE_TAG = "control_size"
    CAPEX_BUDGET_TAG = "capex_budget"
    OPEX_BUDGET_TAG = "opex_budget"

    def __init__(self, config: dict, previous_sol: pd.DataFrame, rich_import: "dict[str, pd.DataFrame]" = None, use_gurobi: bool = False):
        self.__times = {"startup": perf_counter()}
        self.__config = config
        self.__previous_sol = previous_sol.copy()

        # Loading elements
        self.__switches = unmarshalling.load_switches(config)
        self.__microservices = unmarshalling.load_microservices(config)
        self.__hosts = unmarshalling.load_hosts(config)
        self.__workflows = unmarshalling.load_workflows(config)
        self.__links = unmarshalling.load_links(config)
        self.__parameters = self.load_parameters(config)
        self.__rich = rich_import
        if self.__rich is None:
            self.__rich = {
                'z': self.__get_rich_z_vars(previous_sol),
                'f': self.__get_rich_f_vars(previous_sol),
                'fprime': self.__get_rich_fprime_vars(previous_sol),
                'x': self.__get_rich_x_vars(previous_sol),
                'y': self.__get_rich_y_vars(previous_sol),
                'cf': self.__get_rich_cf_vars(previous_sol)
            }
        p_s_dkt = self.__previous_sol.set_index("Variable")["Value"].to_dict()
        self.__z = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('z_')}
        self.__f = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('f_')}
        self.__fprime = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('fprime_')}
        self.__x = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('x_')}
        self.__y = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('y_')}
        self.__cf = {k: p_s_dkt[k] for k in p_s_dkt if k.startswith('cf_')}
        self.__loaded_sol = p_s_dkt
        self.__times['loading'] = perf_counter()

        # Creating dictionaries of elements for convenience
        self.__dkt_switches = self.__dict_by_id(self.__switches)
        self.__dkt_microservices = self.__dict_by_id(self.__microservices)
        self.__dkt_hosts = self.__dict_by_id(self.__hosts)
        self.__dkt_workflows = self.__dict_by_id(self.__workflows)
        self.__dkt_links = self.__dict_links(self.__links)

        # Initializing elements for convenience
        self.__capex = None
        self.__opex = None

        # Quick feasibility checks
        self.__basic_feas_check()

        # Creating the MIP model
        if not use_gurobi:
            self.__mip_model = Model('DADO', solver_name=CBC)
        else:
            self.__mip_model = Model('DADO', solver_name=GUROBI)
        self.__mip_model.threads = -1

        # Creating the MIP variables
        self.__z = self.__create_z_variables()
        self.__f = self.__create_f_variables()
        self.__fprime = self.__create_fprime_variables()
        self.__x = self.__create_x_variables()
        self.__y = self.__create_y_variables()
        self.__cf = self.__create_cf_variables()
        self.__u = None
        self.__rt_preset = None
        self.__times['variable_creation'] = perf_counter()

        # Creating the MIP constraints
        self.__migrate_ms = False
        self.__fixed_ms = []
        self.__migrate_controller = False
        self.__add_single_microservice_constraint()
        if self.__migrate_ms:
            self.__add_memory_constraint()
        self.__add_controller_mapping_constraint()
        if self.__migrate_controller:
            self.__add_active_controller_constraint()
        self.__add_flow_constraints()
        self.__add_ctrl_flow_constraints()
        self.__add_link_capacity_constraint()
        self.__add_max_rt_constraint()
        if self.__config.get(self.CAPEX_BUDGET_TAG):
            self.__u = self.__usage_vars()
            self.__capex = self.__calc_capex()
            self.__add_capex_budget(self.__config[self.CAPEX_BUDGET_TAG])
        if self.__config.get(self.OPEX_BUDGET_TAG):
            if self.__u is None:
                self.__u = self.__usage_vars()
            self.__opex = self.__calc_opex()
            self.__add_opex_budget(self.__config[self.OPEX_BUDGET_TAG])

        self.__times['constraint_creation'] = perf_counter()

        self.__optimized = False
        self.__exportable = False
        self.__suboptimal = False
    
    @staticmethod
    def __get_rich_z_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        z_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'z_')].reset_index(drop=True)
        z_vars['TMP'] = z_vars['Variable'].str.split('_', n=3)
        z_vars['Host'] = z_vars['TMP'].str[1]
        z_vars['Workflow'] = z_vars['TMP'].str[2]
        z_vars['Microservice'] = z_vars['TMP'].str[3]
        z_vars = z_vars.loc[(z_vars['Host'] != 'prime') &
                            (z_vars['Host'] != 'double')]
        z_vars.drop(['TMP'], axis=1, inplace=True)
        return z_vars

    @staticmethod
    def __get_rich_fprime_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        fprime_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'fprime_')].reset_index(drop=True)
        fprime_vars['TMP'] = fprime_vars['Variable'].str.split('_', n=4)
        fprime_vars['Source'] = fprime_vars['TMP'].str[1]
        fprime_vars['Destination'] = fprime_vars['TMP'].str[2]
        fprime_vars['Host'] = fprime_vars['TMP'].str[3]
        fprime_vars['Workflow'] = fprime_vars['TMP'].str[4]
        fprime_vars.drop(['TMP'], axis=1, inplace=True)
        return fprime_vars

    @staticmethod
    def __get_rich_f_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        f_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'f_')].reset_index(drop=True).copy()
        f_vars['TMP'] = f_vars['Variable'].str.split('_', n=5)
        f_vars['Source'] = f_vars['TMP'].str[1]
        f_vars['Destination'] = f_vars['TMP'].str[2]
        f_vars['Host'] = f_vars['TMP'].str[3]
        f_vars['Workflow'] = f_vars['TMP'].str[4]
        f_vars['Microservice'] = f_vars['TMP'].str[5]
        f_vars.drop(['TMP'], axis=1, inplace=True)
        return f_vars
    
    @staticmethod
    def __get_rich_cf_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        cf_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'cf_')].reset_index(drop=True).copy()
        cf_vars['TMP'] = cf_vars['Variable'].str.split('_', n=3)
        cf_vars['Source'] = cf_vars['TMP'].str[1]
        cf_vars['Destination'] = cf_vars['TMP'].str[2]
        cf_vars['Switch'] = cf_vars['TMP'].str[3]
        cf_vars.drop(['TMP'], axis=1, inplace=True)
        return cf_vars

    @staticmethod
    def __get_rich_x_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        x_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'x_')].reset_index(drop=True).copy()
        x_vars['TMP'] = x_vars['Variable'].str.split('_', n=1)
        x_vars['Switch'] = x_vars['TMP'].str[1]
        x_vars.drop(['TMP'], axis=1, inplace=True)
        return x_vars

    @staticmethod
    def __get_rich_y_vars(relevant_vars: pd.DataFrame) -> pd.DataFrame:
        y_vars = relevant_vars.loc[relevant_vars['Variable'].str.startswith(
            'y_')].reset_index(drop=True)
        y_vars['TMP'] = y_vars['Variable'].str.split('_', n=2)
        y_vars['Switch'] = y_vars['TMP'].str[1]
        y_vars['Controller'] = y_vars['TMP'].str[2]
        y_vars.drop(['TMP'], axis=1, inplace=True)
        return y_vars

    @classmethod
    def load_parameters(cls, config: dict) -> dict:
        return config[cls.PARAMETERS_TAG]

    @staticmethod
    def __dict_by_id(elements_with_id: list) -> dict:
        return {element.id: element for element in elements_with_id}

    @staticmethod
    def __dict_links(links: list) -> dict:
        return {(link.source, link.destination): link for link in links}

    def __basic_feas_check(self) -> None:
        total_mem = sum([h.memory for h in self.__hosts])
        total_req = sum(
            [self.__dkt_microservices[m].memory for wf in self.__workflows for m in wf.chain])
        if total_mem > total_req:
            print('Initial feasibility check passed')
        else:
            raise RuntimeError('Feasibility check failed. Total memory: {}. Required memory: {}'.format(
                total_mem, total_req))

    def __create_z_variables(self) -> dict:
        return {
            host.id: {
                workflow.id: {
                    microservice: self.__z.get('z_{}_{}_{}'.format(
                        host.id, workflow.id, microservice), self.__mip_model.add_var('z_{}_{}_{}'.format(
                            host.id, workflow.id, microservice), var_type=BINARY))
                    for microservice in workflow.chain
                }
                for workflow in self.__workflows
            }
            for host in self.__hosts
        }

    def __create_f_variables(self) -> dict:
        return {
            (link.source, link.destination): {
                host.id: {
                    workflow.id: {
                        microservice: self.__f.get('f_{}_{}_{}_{}_{}'.format(
                            link.source, link.destination, host.id, workflow.id, microservice), self.__mip_model.add_var('f_{}_{}_{}_{}_{}'.format(
                                link.source, link.destination, host.id, workflow.id, microservice), var_type=BINARY))
                        for microservice in workflow.chain
                    }
                    for workflow in self.__workflows
                }
                for host in self.__hosts
            }
            for link in self.__links
        }

    def __create_fprime_variables(self) -> dict:
        return {
            (link.source, link.destination): {
                host.id: {
                    workflow.id: self.__fprime.get('fprime_{}_{}_{}_{}'.format(
                        link.source, link.destination, host.id, workflow.id), self.__mip_model.add_var('fprime_{}_{}_{}_{}'.format(
                            link.source, link.destination, host.id, workflow.id), var_type=BINARY))
                    for workflow in self.__workflows if workflow.response
                }
                for host in self.__hosts
            }
            for link in self.__links
        }

    def __create_x_variables(self) -> dict:
        return {
            switch.id: self.__x.get(
                'x_{}'.format(switch.id), self.__mip_model.add_var('x_{}'.format(switch.id), var_type=BINARY))
            for switch in self.__switches
        }

    def __create_y_variables(self) -> dict:
        return {
            switch.id: {
                controller.id: self.__y.get(
                    'y_{}_{}'.format(switch.id, controller.id), self.__mip_model.add_var('y_{}_{}'.format(switch.id, controller.id), var_type=BINARY))
                for controller in self.__switches
            }
            for switch in self.__switches
        }

    def __create_cf_variables(self) -> dict:
        return {
            (link.source, link.destination): {
                switch.id: self.__cf.get('cf_{}_{}_{}'.format(
                    link.source, link.destination, switch.id), self.__mip_model.add_var('cf_{}_{}_{}'.format(
                        link.source, link.destination, switch.id), var_type=BINARY))
                for switch in self.__switches
            }
            for link in self.__links
        }

    def __add_single_microservice_constraint(self) -> None:
        for t in self.__workflows:
            microservices = t.chain
            for m in microservices:
                if len(self.__rich['z'].query('Workflow == @t.id and Microservice == @m and Value > 0')) == 0:
                    self.__migrate_ms = True
                    self.__mip_model.add_constr(xsum(self.__z[h.id][t.id][m] for h in self.__hosts)
                                                == 1, 'max_mapping_{}_{}'.format(t.id, m))
                else:
                    self.__fixed_ms.append((t.id, m))

    def __add_memory_constraint(self) -> None:
        for h in self.__hosts:
            self.__mip_model.add_constr(xsum(self.__z[h.id][t.id][m]*self.__dkt_microservices[m].memory
                                             for t in self.__workflows for m in t.chain) <= h.memory, 'max_power_{}'.format(h.id))

    """ def __add_single_thread_constraint(self) -> None:
        for h in self.__hosts:
            for t in self.__workflows:
                for m in t.chain:
                    self.__mip_model.add_constr(self.__z[h.id][t.id][m]*self.__dkt_microservices[m].cycles
                    <= h.power, 'single_core_{}_{}_{}'.format(h.id, t.id, m)) """

    def __add_maximum_controllers_constraint(self) -> None:
        if self.CONTROLLERS_TAG in self.__parameters:
            if len(self.__rich['x'].query("Value > 0")) < self.__parameters[self.CONTROLLERS_TAG]:
                self.__mip_model.add_constr(xsum(self.__x[s.id] for s in self.__switches)
                                            <= self.__parameters[self.CONTROLLERS_TAG], 'maximum_controllers')

    def __add_controller_mapping_constraint(self) -> None:
        for s in self.__switches:
            if len(self.__rich['y'].query("Switch == @s.id and Value > 0")) == 0:
                self.__migrate_controller = True
                self.__mip_model.add_constr(xsum(
                    self.__y[s.id][c.id] for c in self.__switches) == 1, 'ctrl_mapping_{}'.format(s.id))

    def __add_active_controller_constraint(self) -> None:
        for s in self.__switches:
            if len(self.__rich['y'].query("Switch == @s.id and Value > 0")) == 0:
                for c in self.__switches:
                    self.__mip_model.add_constr(
                        self.__y[s.id][c.id] <= self.__x[c.id], 'active_controller_{}_{}'.format(s.id, c.id))

    def __add_flow_constraints(self) -> None:
        # Starter flow

        for t in self.__workflows:
            m0 = t.chain[0]
            if len(self.__rich['f'].query("Microservice == @m0 and Workflow == @t.id and Value > 0")) == 0:
                for i in self.__hosts + self.__switches:
                    j_adj = list(map(lambda l: l.destination, filter(
                        lambda ll: ll.source == i.id, self.__links)))
                    for h in self.__hosts:
                        flow_linexp = xsum(self.__f[(
                            i.id, j)][h.id][t.id][m0] - self.__f[(j, i.id)][h.id][t.id][m0] for j in j_adj)
                        if i in self.__switches:
                            self.__mip_model.add_constr(
                                flow_linexp == 0, 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m0))
                        else:
                            if i.id == h.id:
                                self.__mip_model.add_constr(flow_linexp == t.is_starter(
                                    h)*(1-self.__z[i.id][t.id][m0]), 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m0))
                            else:
                                self.__mip_model.add_constr(flow_linexp == -1*t.is_starter(
                                    h)*self.__z[i.id][t.id][m0], 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m0))

        # Response flow
        for t in self.__workflows:
            if t.response and len(self.__rich['fprime'].query("Workflow == @t.id and Value > 0")) == 0:
                mn = t.chain[-1]
                for i in self.__hosts + self.__switches:
                    j_adj = list(map(lambda l: l.destination, filter(
                        lambda ll: ll.source == i.id, self.__links)))
                    for h in self.__hosts:
                        flow_prime_linexp = xsum(self.__fprime[(
                            i.id, j)][h.id][t.id] - self.__fprime[(j, i.id)][h.id][t.id] for j in j_adj)
                        if i in self.__switches:
                            self.__mip_model.add_constr(
                                flow_prime_linexp == 0, 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, mn))
                        else:
                            if i.id == h.id:
                                self.__mip_model.add_constr(flow_prime_linexp == self.__z[h.id][t.id][mn]*(
                                    1-t.is_starter(i))*t.response_type(), 'flow_prime_{}_{}_{}'.format(i.id, h.id, t.id))
                            else:
                                self.__mip_model.add_constr(flow_prime_linexp == -1*self.__z[h.id][t.id][mn]*t.is_starter(
                                    i)*t.response_type(), 'flow_prime_{}_{}_{}'.format(i.id, h.id, t.id))

        # General flow
        for t in self.__workflows:
            for m, m_minus in zip(t.chain[1:], t.chain):
                if len(self.__rich['f'].query("Microservice == @m and Workflow == @t.id and Value > 0")) == 0:
                    for i in self.__hosts + self.__switches:
                        j_adj = list(map(lambda l: l.destination, filter(
                            lambda ll: ll.source == i.id, self.__links)))
                        for h in self.__hosts:
                            flow_gen_linexp = xsum(self.__f[(
                                i.id, j)][h.id][t.id][m] - self.__f[(j, i.id)][h.id][t.id][m] for j in j_adj)
                            if i in self.__switches:
                                self.__mip_model.add_constr(
                                    flow_gen_linexp == 0, 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m))
                            else:
                                if i.id == h.id:
                                    cheat_z_var_1 = self.__mip_model.add_var(
                                        'z_prime_{}_{}_{}_{}'.format(h.id, i.id, t.id, m), var_type=BINARY)
                                    self.__mip_model.add_constr(
                                        -1*self.__z[h.id][t.id][m_minus]+cheat_z_var_1 <= 0)
                                    self.__mip_model.add_constr(
                                        -1+self.__z[i.id][t.id][m]+cheat_z_var_1 <= 0)
                                    self.__mip_model.add_constr(
                                        self.__z[h.id][t.id][m_minus]+1-self.__z[i.id][t.id][m]-cheat_z_var_1 <= 1)
                                    self.__mip_model.add_constr(
                                        flow_gen_linexp == cheat_z_var_1, 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m))
                                else:
                                    cheat_z_var_2 = self.__mip_model.add_var(
                                        'z_double_{}_{}_{}_{}'.format(h.id, i.id, t.id, m), var_type=BINARY)
                                    self.__mip_model.add_constr(
                                        -1*self.__z[h.id][t.id][m_minus]+cheat_z_var_2 <= 0)
                                    self.__mip_model.add_constr(
                                        -1*self.__z[i.id][t.id][m]+cheat_z_var_2 <= 0)
                                    self.__mip_model.add_constr(
                                        self.__z[h.id][t.id][m_minus]+self.__z[i.id][t.id][m]-cheat_z_var_2 <= 1)
                                    self.__mip_model.add_constr(
                                        flow_gen_linexp == -1*cheat_z_var_2, 'flow_{}_{}_{}_{}'.format(i.id, h.id, t.id, m))

    def __add_ctrl_flow_constraints(self):
        for s in self.__switches:
            if len(self.__rich['cf'].query("Switch == @s.id and Value > 0")) == 0:
                for i in self.__hosts + self.__switches:
                    j_adj = list(map(lambda l: l.destination, filter(
                        lambda ll: ll.source == i.id, self.__links)))
                    ctrl_flow_linexp = xsum(
                        self.__cf[(i.id, j)][s.id] - self.__cf[(j, i.id)][s.id] for j in j_adj)
                    if i in self.__hosts:
                        self.__mip_model.add_constr(
                            ctrl_flow_linexp == 0, 'ctrl_flow_{}_{}'.format(i.id, s.id))
                    else:
                        if i.id == s.id:
                            self.__mip_model.add_constr(
                                ctrl_flow_linexp == 1 - self.__y[s.id][i.id], 'ctrl_flow_{}_{}'.format(i.id, s.id))
                        else:
                            self.__mip_model.add_constr(
                                ctrl_flow_linexp == -1*self.__y[s.id][i.id], 'ctrl_flow_{}_{}'.format(i.id, s.id))

    def __add_link_capacity_constraint(self):
        for l in self.__links:
            self.__mip_model.add_constr(
                xsum(
                    xsum(
                        self.__f[(l.source, l.destination)][h.id][t.id][m] *
                        self.__dkt_microservices[m].input
                        for m in t.chain
                    ) +
                    xsum(
                        self.__cf[(l.source, l.destination)][s.id] *
                        self.__parameters[self.CONTROL_SIZE_TAG]
                        for s in self.__switches
                    )
                    +
                    self.__fprime.get((l.source, l.destination), {}).get(h.id, {}).get(t.id, 0) *
                    self.__dkt_microservices[t.chain[-1]].output
                    for h in self.__hosts for t in self.__workflows

                ) <= l.capacity,
                'capacity_{}_{}'.format(l.source, l.destination))
    
    def __add_max_rt_constraint(self):
        self.__rt_preset = []
        for t in self.__workflows:
            wf_rt = 0
            for m in t.chain:
                if (t.id, m) not in self.__fixed_ms:
                    for h in self.__hosts: # Execution time
                        wf_rt += (self.__z[h.id][t.id][m]*self.__dkt_microservices[m].cycles)/h.power
                for h in self.__hosts:
                    for l in self.__links: # Traffic latency
                        wf_rt += self.__f[(l.source, l.destination)][h.id][t.id][m]*l.latency
                        if l.destination in self.__dkt_switches: # Control latency derived from traffic flows
                            wf_rt += xsum(self.__cf[l2.source, l2.destination][l.destination]*l2.latency for l2 in self.__links)
            if t.response:
                for h in self.__hosts:
                    for l in self.__links: # Response traffic latency
                        wf_rt += self.__fprime[(l.source, l.destination)][h.id][t.id]*l.latency
                        if l.destination in self.__dkt_switches:  # Control latency derived from response flows
                            wf_rt += xsum(self.__cf[l2.source, l2.destination][l.destination]*l2.latency for l2 in self.__links)
            if t.max_response_time > 0:
                self.__mip_model.add_constr(wf_rt <= t.max_response_time, 'qos_{}'.format(t.id))
            self.__rt_preset.append(wf_rt)

    def __usage_vars(self) -> dict:
        usage_vars = {"links": {}, "switches": {},
                      "hosts": {}, "cycles": {}, "memory": {}}
        for l in self.__links:
            usage_vars["links"][(l.source, l.destination)] = self.__mip_model.add_var(
                'u_l_{}_{}'.format(l.source, l.destination), var_type=BINARY)
            for h_id in self.__f[(l.source, l.destination)]:
                for t_id in self.__f[(l.source, l.destination)][h_id]:
                    for m in self.__f[(l.source, l.destination)][h_id][t_id]:
                        self.__mip_model.add_constr(usage_vars["links"][(
                            l.source, l.destination)] >= self.__f[(l.source, l.destination)][h_id][t_id][m])
                    self.__mip_model.add_constr(usage_vars["links"][(
                        l.source, l.destination)] >= self.__fprime[(l.source, l.destination)][h_id][t_id])
            for s_id in self.__cf[(l.source, l.destination)]:
                self.__mip_model.add_constr(usage_vars["links"][(
                    l.source, l.destination)] >= self.__cf[(l.source, l.destination)][s_id])
        for s in self.__switches:
            usage_vars["switches"][s.id] = self.__mip_model.add_var(
                'u_s_{}'.format(s.id), var_type=BINARY)
            for l in list(filter(lambda x: x.source == s.id, self.__links)):
                for h_id in self.__f[(l.source, l.destination)]:
                    for t_id in self.__f[(l.source, l.destination)][h_id]:
                        for m in self.__f[(l.source, l.destination)][h_id][t_id]:
                            self.__mip_model.add_constr(usage_vars["switches"][
                                s.id] >= self.__f[(l.source, l.destination)][h_id][t_id][m])
                        self.__mip_model.add_constr(usage_vars["switches"][
                            s.id] >= self.__fprime[(l.source, l.destination)][h_id][t_id])
                for t_id in self.__cf[(l.source, l.destination)]:
                    self.__mip_model.add_constr(
                        usage_vars["switches"][s.id] >= self.__cf[(l.source, l.destination)][t_id])
        for h in self.__hosts:
            usage_vars["hosts"][h.id] = self.__mip_model.add_var(
                'u_h_{}'.format(h.id), var_type=BINARY)
            for t_id in self.__z[h.id]:
                for m in self.__z[h.id][t_id]:
                    self.__mip_model.add_constr(
                        usage_vars["hosts"][h.id] >= self.__z[h.id][t_id][m])
            usage_vars["cycles"][h.id] = xsum(
                self.__z[h.id][t.id][m]*self.__dkt_microservices[m].cycles for t in self.__workflows for m in t.chain)
            usage_vars["memory"][h.id] = xsum(
                self.__z[h.id][t.id][m]*self.__dkt_microservices[m].memory for t in self.__workflows for m in t.chain)
        return usage_vars

    def __calc_capex(self) -> LinExpr:
        if self.__u is None:
            self.__u = self.__usage_vars()
        net_capex = xsum(self.__u["links"][(l.source, l.destination)]*l.capex for l in self.__links) + \
            xsum(self.__u["switches"][s.id]*s.capex for s in self.__switches)
        control_capex = xsum(
            self.__x[s.id]*s.capex_cnt for s in self.__switches)
        deploy_capex = xsum(self.__u["hosts"]
                            [h.id]*h.capex for h in self.__hosts)
        capex = net_capex+control_capex+deploy_capex
        return capex

    def __calc_opex(self) -> LinExpr:
        if self.__u is None:
            self.__u = self.__usage_vars()
        net_opex = xsum(self.__u["links"][(l.source, l.destination)]*l.opex for l in self.__links) + xsum(
            self.__u["switches"][s.id]*s.opex for s in self.__switches)
        control_opex = xsum(self.__x[s.id]*s.opex_cnt for s in self.__switches)
        deploy_opex = xsum(self.__u["cycles"][h.id]*h.opex_cycle +
                           self.__u["memory"][h.id]*h.opex_memory for h in self.__hosts)
        opex = net_opex+control_opex+deploy_opex
        return opex

    def __add_capex_budget(self, budget: float):
        if self.__capex is None:
            self.__capex = self.__calc_capex()

        self.__mip_model.add_constr(self.__capex <= budget, "capex_budget")

    def __add_opex_budget(self, budget: float):
        if self.__opex is None:
            self.__opex = self.__calc_opex()

        self.__mip_model.add_constr(self.__opex <= budget, "opex_budget")

    def objective_avg_response_time(self):
        if self.__migrate_controller:
            self.__add_maximum_controllers_constraint()
        if self.__rt_preset is not None:
            self.__mip_model.objective = minimize(xsum(self.__rt_preset))
        else:
            all_wf_rt = []
            for t in self.__workflows:
                wf_rt = 0
                for m in t.chain:
                    if (t.id, m) not in self.__fixed_ms:
                        for h in self.__hosts: # Execution time
                            wf_rt += (self.__z[h.id][t.id][m]*self.__dkt_microservices[m].cycles)/h.power
                    for h in self.__hosts:
                        for l in self.__links: # Traffic latency
                            wf_rt += self.__f[(l.source, l.destination)][h.id][t.id][m]*l.latency
                            if l.destination in self.__dkt_switches: # Control latency derived from traffic flows
                                wf_rt += xsum(self.__cf[l2.source, l2.destination][l.destination]*l2.latency for l2 in self.__links)
                if t.response:
                    for h in self.__hosts:
                        for l in self.__links: # Response traffic latency
                            wf_rt += self.__fprime[(l.source, l.destination)][h.id][t.id]*l.latency
                            if l.destination in self.__dkt_switches:  # Control latency derived from response flows
                                wf_rt += xsum(self.__cf[l2.source, l2.destination][l.destination]*l2.latency for l2 in self.__links)
                all_wf_rt.append(wf_rt)
            
            self.__mip_model.objective = minimize(xsum(all_wf_rt))
        self.__times['objective_time'] = perf_counter()

    def save_debug(self, out_filename: str = 'TempDADO.lp') -> None:
        self.__mip_model.write(out_filename)
    
    def save_prev_sol_debug(self, out_filename: str = 'TempDADOSol.csv') -> None:
        self.__previous_sol.to_csv(out_filename, index=False)
    
    
    def execute(self, time_limit: int = None, use_gurobi: bool = False):
        if time_limit is not None:
            if not use_gurobi:
                opt_status = self.__mip_model.optimize(max_seconds=time_limit)
            else:
                opt_status = self.__mip_model.optimize(
                    max_seconds=time_limit, solver_name=GUROBI)
        else:
            if not use_gurobi:
                opt_status = self.__mip_model.optimize()
            else:
                opt_status = self.__mip_model.optimize(solver_name=GUROBI)
        if opt_status == OptimizationStatus.NO_SOLUTION_FOUND or opt_status == OptimizationStatus.INFEASIBLE:
            print('Infeasible problem!!!')
        else:
            self.__times['optimized'] = perf_counter()
            self.__exportable = True
            if opt_status != OptimizationStatus.OPTIMAL:
                self.__suboptimal = True
        self.__optimized = True

    def preset_vars(self, var_vals: dict):
        for var in var_vals:
            m_var = self.__mip_model.var_by_name(var)
            self.__mip_model.add_constr(
                m_var == var_vals[var], 'accel_{}'.format(var))

    def get_solution(self) -> DataFrame:
        if not self.__optimized:
            self.execute()
        if self.__exportable:
            sol = [{"Variable": k, "Value": self.__loaded_sol[k]} for k in self.__loaded_sol]
            for var in self.__mip_model.vars:
                sol.append({"Variable": var.name, "Value": var.x})
            return pd.DataFrame(sol)
        else:
            raise RuntimeError(
                'Trying to export solution of infeasible problem')

    def get_timing_report(self) -> dict:
        report = {}
        if 'loading' in self.__times:
            report['Scenario load'] = self.__times['loading'] - \
                self.__times['startup']
            if 'variable_creation' in self.__times:
                report['Variable creation'] = self.__times['variable_creation'] - \
                    self.__times['loading']
                if 'constraint_creation' in self.__times:
                    report['Constraint creation'] = self.__times['constraint_creation'] - \
                        self.__times['variable_creation']
                    if 'objective_time' in self.__times:
                        report['Objective creation'] = self.__times['objective_time'] - \
                            self.__times['constraint_creation']
        if 'optimized' in self.__times:
            if 'objective_time' in self.__times:
                report['Optimization'] = self.__times['optimized'] - \
                    self.__times['objective_time']
            report['Full process'] = self.__times['optimized'] - \
                self.__times['startup']
        if self.__suboptimal:
            report['Suboptimal solution!'] = 1.0
        return report


class CommandUI:

    def __init__(self):
        self.__parser = argparse.ArgumentParser(
            description='Continuous DADO MIP Optimizer')
        self.__init_parser()

    def __init_parser(self):
        self.__parser.add_argument(
            '-ip', required=True, metavar='Previous DADOJSON config', help="Previous status of the case study or config in DADOJSON format")
        self.__parser.add_argument(
            '-sp', required=True, metavar='Previous CSV solution', help='Previous solution for the deployment in CSV format')
        self.__parser.add_argument(
            '-i', required=True, metavar='DADOJSON config', help='Case study or config in DADOJSON format')
        self.__parser.add_argument('-o', required=False, metavar='CSV solution',
                                   help='File to output the solution to in CSV format')
        self.__parser.add_argument('-trep', required=False, metavar='CSV timing report',
                                   help='File to output the timing report to in CSV format')
        self.__parser.add_argument('-limit', required=False, metavar='Time limit',
                                   help='Maximum time for optimization (in seconds)', type=int)
        self.__parser.add_argument('-debug', action='store_true', help='Enable debug')
        self.__parser.add_argument(
            '-grb', action='store_true', help='Use Gurobi MILP solver (commercial, external) instead of Coin-Or Branch (free, built-in)')

    def launch(self):
        from file_io import ConfigLoader, TimingReport
        from DADOPartialSolutioner import PartialSolutioner
        l_args = self.__parser.parse_args()
        if l_args.o:
            import os
            import sys
            if os.path.exists(l_args.o):
                sys.exit()
        part_sol = PartialSolutioner(l_args.ip, l_args.i, l_args.sp)
        loader = ConfigLoader(l_args.i)
        config = loader.load_json_config()
        part_sol.invalidate()
        ps = part_sol.export_valid_solution(l_args.debug)
        dado_model = PartialSolDADO(config, ps, part_sol.export_rich_solution_vars(), use_gurobi = l_args.grb)
        dado_model.objective_avg_response_time()
        if l_args.debug:
            dado_model.save_debug()
            dado_model.save_prev_sol_debug()
        if l_args.limit:
            dado_model.execute(l_args.limit)
        else:
            dado_model.execute()
        if l_args.o:
            dado_model.get_solution().to_csv(l_args.o, index=False)
        if l_args.trep:
            t_report = TimingReport(l_args.trep)
            t_report.export_as_csv(dado_model.get_timing_report())


if __name__ == "__main__":
    CommandUI().launch()
