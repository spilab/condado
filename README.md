# ConDADO prototype

This project contains the Continuous DADO (ConDADO) prototype, as presented in the paper _Continuous QoS-Aware Deployment Adaptation for Next Generation IoT_, as well as the use case from the evaluation.

## Requirements for the use of ConDADO

- Python 3.7.5 (or higher).
- `pip` Python package manager.
- `pandas`, `matplotlib`, and `networkx` Python packages (installation instructions below).
- `mip` Python package, ver. 1.8.2 (installation instructions below).
- Gurobi solver (recommended).

## Installing ConDADO's dependencies

### Windows

```bash
pip install -r requirements.txt
```

### Linux and macOS

```bash
pìp3 install -r requirements.txt
```

## Using ConDADO

The usage of ConDADO is as follows:

```bash
python condado.py -ip <Previous DADOJSON> -sp <Previous solution> -i <Current DADOJSON> [-o <Solution output>] [-trep <Timing report output>] [-limit <Time limit for the solver>] [-debug] [-grb]
```

### Arguments

| Argument | Description | Format | Type |
| --- | --- | --- | --- |
| Previous DADOJSON | Configuration from the previous time instant. | File path (\*.dadojson) | Mandatory |
| Previous solution | Application placement from the previous time instant. | File path (\*.csv) | Mandatory |
| Current DADOJSON | Configuration from the current time instant. | File path (\*.dadojson) | Mandatory |
| Solution output | Application placement determined by ConDADO. | File path (\*.csv) | Optional |
| Timing report output | Report of time taken per step by ConDADO | File path (\*.csv) | Optional |
| Time limit | Limit of time for the MILP solver to optimize. | Integer | Optional |
| `-debug` | Enable debug mode | N/A | Optional
| `-grb` | Use Gurobi solver instead of built-in Coin-OR Branch | N/A | Optional |