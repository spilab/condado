import json
import networkx as nx
from typing import Union, Any

class ChangeAnalyzer:

    def __init__(self, prev_dadojson: "Union[str, dict]", curr_dadojson: "Union[str, dict]") -> None:
        if type(prev_dadojson) == str:
            with open(prev_dadojson, 'r') as in_json:
                prev_setup = json.load(in_json)
        else:
            prev_setup = prev_dadojson
        if type(curr_dadojson) == str:
            with open(curr_dadojson, 'r') as in_json:
                curr_setup = json.load(in_json)
        else:
            curr_setup = curr_dadojson
        self.__prev_setup = prev_setup
        self.__curr_setup = curr_setup
        self.__prev_topo = self.__build_topology(prev_setup)
        self.__curr_topo = self.__build_topology(curr_setup)
    
    @staticmethod
    def __build_topology(setup: dict) -> nx.Graph:
        setup_links = setup["links"]
        topo = nx.Graph()
        for link in setup_links:
            topo.add_edge(link["source"], link["destination"], latency=link.get("latency", 0), 
            capacity=link.get("capacity", 1e10), capex=link.get("capex", 0), opex=link.get("opex", 0))
        return topo
    
    @staticmethod
    def __compare_topos(topo_a: nx.Graph, topo_b: nx.Graph) -> "dict[str, dict[str, list[Union[str, dict[str, Union[str, float]]]]]]":
        added_nodes, removed_nodes = [], []
        for node in topo_a.nodes:
            if node not in topo_b.nodes:
                removed_nodes.append(node)
        for node in topo_b.nodes:
            if node not in topo_a.nodes:
                added_nodes.append(node)
        added_links, removed_links, modified_links = [], [], []
        for edge in topo_a.edges:
            if topo_b.has_edge(edge[0], edge[1]):
                link_modifiers = {}
                if topo_a.edges[edge].get("latency", 0) != topo_b.edges[edge].get("latency", 0):
                    link_modifiers["latency"] = topo_b.edges[edge].get(
                        "latency", 0) - topo_a.edges[edge].get("latency", 0)
                if topo_a.edges[edge].get("capacity", 1e10) != topo_b.edges[edge].get("capacity", 1e10):
                    link_modifiers["capacity"] = topo_b.edges[edge].get(
                        "capacity", 1e10) - topo_a.edges[edge].get("capacity", 1e10)
                if topo_a.edges[edge].get("capex", 0) != topo_b.edges[edge].get("capex", 0):
                    link_modifiers["capex"] = topo_b.edges[edge].get(
                        "capex", 0) - topo_a.edges[edge].get("capex", 0)
                if topo_a.edges[edge].get("opex", 0) != topo_b.edges[edge].get("opex", 0):
                    link_modifiers["opex"] = topo_b.edges[edge].get(
                        "opex", 0) - topo_a.edges[edge].get("opex", 0)
                if len(link_modifiers.keys()) > 0:
                    link_modifiers["link"] = edge
                    modified_links.append(link_modifiers)
            else:
                removed_links.append(edge)
        for edge in topo_b.edges:
            if not topo_a.has_edge(edge[0], edge[1]):
                added_links.append(edge)
        return {"added": {"nodes": added_nodes, "links": added_links}, 
                "modified": {"links": modified_links},
                "removed": {"nodes": removed_nodes, "links": removed_links}}
    
    @staticmethod
    def __compare_workflows(workflows_a: "list[dict[str, Union[bool, str, list[str]]]]", workflows_b: "list[dict[str, Union[bool, str, list[str]]]]") -> "dict[str, dict[str, list[Union[str, dict[str, Union[str, bool, list[str]]]]]]]":
        wf_a_by_id = {wf["id"]: wf for wf in workflows_a}
        wf_b_by_id = {wf["id"]: wf for wf in workflows_b}
        added_workflows, removed_workflows, modified_workflows = [], [], []
        for wf_id in wf_a_by_id:
            if wf_id not in wf_b_by_id:
                removed_workflows.append(wf_id)
            else:
                wf_a = wf_a_by_id[wf_id]
                wf_b = wf_b_by_id[wf_id]
                wf_modifiers = {}
                if wf_a.get("response", False) != wf_b.get("response", False):
                    wf_modifiers["response"] = wf_b.get("response", False)
                if wf_a["starter"] != wf_b["starter"]:
                    wf_modifiers["starter"] = wf_b["starter"]
                for m_a, m_b in zip(wf_a["chain"], wf_b["chain"]):
                    if m_a != m_b:
                        wf_modifiers["chain"] = wf_b["chain"]
                        break
                if len(wf_modifiers.keys()) > 0:
                    wf_modifiers["workflow"] = wf_id
                    modified_workflows.append(wf_modifiers)
        
        for wf_id in wf_b_by_id:
            if wf_id not in wf_a_by_id:
                added_workflows.append(wf_id)
        
        return {"added": {"workflows": added_workflows}, 
                "modified": {"workflows": modified_workflows},
                "removed": {"workflows": removed_workflows}}
    
    @staticmethod
    def __compare_microservices(microservices_a: "list[dict[str, Union[str, float]]]", microservices_b: "list[dict[str, Union[str, float]]]") -> "dict[str, dict[str, list[Union[str, dict[str, Union[str, float]]]]]]":
        ms_a_by_id = {ms["id"]: ms for ms in microservices_a}
        ms_b_by_id = {ms["id"]: ms for ms in microservices_b}
        added_microservices, removed_microservices, modified_microservices = [], [], []
        for ms_id in ms_a_by_id:
            if ms_id not in ms_b_by_id:
                removed_microservices.append(ms_id)
            else:
                ms_a = ms_a_by_id[ms_id]
                ms_b = ms_b_by_id[ms_id]
                ms_modifiers = {}
                if ms_a.get("cycles", 0) != ms_b.get("cycles", 0):
                    ms_modifiers["cycles"] = ms_b.get("cycles", 0) - ms_a.get("cycles", 0)
                if ms_a.get("memory", 0) != ms_b.get("memory", 0):
                    ms_modifiers["memory"] = ms_b.get("memory", 0) - ms_a.get("memory", 0)
                if ms_a.get("input", 0) != ms_b.get("input", 0):
                    ms_modifiers["input"] = ms_b.get("input", 0) - ms_a.get("input", 0)
                if ms_a.get("output", 0) != ms_b.get("output", 0):
                    ms_modifiers["output"] = ms_b.get("output", 0) - ms_a.get("output", 0)
                if len(ms_modifiers.keys()) > 0:
                    ms_modifiers["microservice"] = ms_id
                    modified_microservices.append(ms_modifiers)
        
        for ms_id in ms_b_by_id:
            if ms_id not in ms_a_by_id:
                added_microservices.append(ms_id)
        
        return {"added": {"microservices": added_microservices},
                "modified": {"microservices": modified_microservices},
                "removed": {"microservices": removed_microservices}}

    @staticmethod
    def __compare_hosts(hosts_a: "list[dict[str, Union[str, float]]]", hosts_b: "list[dict[str, Union[str, float]]]") -> "dict[str, dict[str, list[Union[str, dict[str, Union[str, float]]]]]]":
        h_by_id_a = {h["id"]: h for h in hosts_a}
        h_by_id_b = {h["id"]: h for h in hosts_b}

        modified_hosts = []

        for h_id in h_by_id_a:
            if h_id in h_by_id_b:
                h_a = h_by_id_a[h_id]
                h_b = h_by_id_b[h_id]
                h_modifications = {}
                if h_a.get("power", 1) != h_b.get("power", 1):
                    h_modifications["power"] = h_b.get("power", 1) - h_a.get("power", 1)
                if h_a.get("memory", 0) != h_b.get("memory", 0):
                    h_modifications["memory"] = h_b.get("memory", 0) - h_a.get("memory", 0)
                if h_a.get("capex", 0) != h_b.get("capex", 0):
                    h_modifications["capex"] = h_b.get("capex", 0) - h_a.get("capex", 0)
                if h_a.get("opex_cycle", 0) != h_b.get("opex_cycle", 0):
                    h_modifications["opex_cycle"] = h_b.get("opex_cycle", 0) - h_a.get("opex_cycle", 0)
                if h_a.get("opex_memory", 0) != h_b.get("opex_memory", 0):
                    h_modifications["opex_memory"] = h_b.get("opex_memory", 0) - h_a.get("opex_memory", 0)
                if len(h_modifications.keys()) > 0:
                    h_modifications["host"] = h_id
                    modified_hosts.append(h_modifications)
        
        return {"modified": {"hosts": modified_hosts}}
    
    @staticmethod
    def __compare_switches(switches_a: "list[dict[str, Union[str, float]]]", switches_b: "list[dict[str, Union[str, float]]]") -> "dict[str, dict[str, list[Union[str, dict[str, Union[str, float]]]]]]":
        s_by_id_a = {s["id"]: s for s in switches_a}
        s_by_id_b = {s["id"]: s for s in switches_b}

        modified_switches = []

        for s_id in s_by_id_a:
            if s_id in s_by_id_b:
                s_a = s_by_id_a[s_id]
                s_b = s_by_id_b[s_id]
                s_modifications = {}
                if s_a.get("capex", 0) != s_b.get("capex", 0):
                    s_modifications["capex"] = s_b.get("capex", 0) - s_a.get("capex", 0)
                if s_a.get("opex", 0) != s_b.get("opex", 0):
                    s_modifications["opex"] = s_b.get("opex", 0) - s_a.get("opex", 0)
                if s_a.get("capex_cnt", 0) != s_b.get("capex_cnt", 0):
                    s_modifications["capex_cnt"] = s_b.get("capex_cnt", 0) - s_a.get("capex_cnt", 0)
                if s_a.get("opex_cnt", 0) != s_b.get("opex_cnt", 0):
                    s_modifications["opex_cnt"] = s_b.get("opex_cnt", 0) - s_a.get("opex_cnt", 0)
                if len(s_modifications.keys()) > 0:
                    s_modifications["switch"] = s_id
                    modified_switches.append(s_modifications)

        return {"modified": {"switches": modified_switches}}
    
    @staticmethod
    def __compare_parameters(params_a: "dict[str, Any]", params_b: "dict[str, Any]") -> "dict[str, dict[str, Union[list[str], dict[str, Any]]]]":
        added_params, modified_params, removed_params = [], [], []
        for param in params_a:
            if param not in params_b:
                removed_params.append(param)
            else:
                par_a = params_a[param]
                par_b = params_b[param]
                if (type(par_a) == int or type(par_a) == float) and par_b != par_a:
                    modified_params.append({param: par_b - par_a})
                else:
                    modified_params.append({param: par_b})
        
        for param in params_b:
            if param not in params_a:
                added_params.append(param)
        
        return {"added": {"parameters": added_params},
                "modified": {"parameters": modified_params},
                "removed": {"parameters": removed_params}}
    
    def get_comparison(self) -> "dict[str, dict[str, list[Union[str, dict[str, Any]]]]]":
        cmp_topo = self.__compare_topos(self.__prev_topo, self.__curr_topo)
        all_hosts_ids = list(dict.fromkeys([h["id"] for h in self.__prev_setup["hosts"] + self.__curr_setup["hosts"]]))
        for category in ["added", "removed"]:
            for id in cmp_topo[category]["nodes"]:
                if id in all_hosts_ids:
                    cmp_topo[category]["hosts"] = cmp_topo[category].get("hosts", []) + [id]
                else:
                    cmp_topo[category]["switches"] = cmp_topo[category].get("switches", []) + [id]
            cmp_topo[category].pop("nodes")
        cmp_hosts = self.__compare_hosts(self.__prev_setup["hosts"], self.__curr_setup["hosts"])
        cmp_switches = self.__compare_switches(self.__prev_setup["switches"], self.__curr_setup["switches"])
        cmp_microservices = self.__compare_microservices(self.__prev_setup["microservices"], self.__curr_setup["microservices"])
        cmp_workflows = self.__compare_workflows(self.__prev_setup["workflows"], self.__curr_setup["workflows"])
        cmp_parameters = self.__compare_parameters(self.__prev_setup["parameters"], self.__curr_setup["parameters"])
        comparisons = [cmp_topo, cmp_hosts, cmp_switches, cmp_microservices, cmp_workflows, cmp_parameters]
        all_cmp = {}
        for category in ["added", "removed", "modified"]:
            all_cmp[category] = {}
            for cmp_type in comparisons:
                for element_type in cmp_type.get(category, []):
                    all_cmp[category][element_type] = cmp_type[category][element_type]
        return all_cmp